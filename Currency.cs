﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask_Currency
{
	public class Currency
	{
		public string Name { get; set; }
		public decimal Rate { get; set; }
		public string Info { get; set; }
	}
}
