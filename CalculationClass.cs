﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask_Currency
{
	public static class CalculationClass
	{
		public static string Calculate (List<Currency> currencyList, string validString)
		{
			CultureInfo cultures = new CultureInfo("en-US");
			string answer = "";

			#region parsingString
			decimal currencyMain = validString.Substring(0, 3) == "DKK" ? 100m : 
				currencyList.Where(p => p.Name == validString.Substring(0, 3)).Select(p => p.Rate).FirstOrDefault();
			decimal currencyMoney = validString.Substring(4, 3) == "DKK" ? 100m : 
				currencyList.Where(p => p.Name == validString.Substring(4, 3)).Select(p => p.Rate).FirstOrDefault();
			string sumInput = validString.Substring(8, validString.Length - 8);
			#endregion

			try
			{
				answer = ((Decimal.Parse(sumInput, cultures) * currencyMain) / currencyMoney).ToString(cultures);
			}
			catch (Exception)
			{
				answer = "ERROR! Bad input: " + sumInput;
			}
			
			return answer;
		}
	}
}
