﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask_Currency
{
	public class CurrencyLoader
	{
		List<Currency> currencyList = new List<Currency>();

		public List<Currency> GetCurrencies ()
		{
			#region fillingList
			currencyList.Add(new Currency() { Name = "EUR", Rate = 743.94m, Info = "Euro" });
			currencyList.Add(new Currency() { Name = "USD", Rate = 663.11m, Info = "United States Dollar" });
			currencyList.Add(new Currency() { Name = "GBP", Rate = 852.85m, Info = "Pound Sterling" });
			currencyList.Add(new Currency() { Name = "SEK", Rate = 76.10m, Info = "Swedish Krona" });
			currencyList.Add(new Currency() { Name = "NOK", Rate = 78.40m, Info = "Norwegian Krona" });
			currencyList.Add(new Currency() { Name = "CHF", Rate = 683.58m, Info = "Swiss Franc" });
			currencyList.Add(new Currency() { Name = "JPY", Rate = 59740m, Info = "Japanese Yen" });
			#endregion

			return currencyList;
		}

		public List<string> GetCurrencyNameInfoList()
		{
			return currencyList.Select(p => p.Info + ": " + p.Name).ToList();
		}
	}
}
