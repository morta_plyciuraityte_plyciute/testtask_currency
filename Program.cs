﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestTask_Currency
{
	class Program
	{
		static void Main()
		{
			#region strings
			string input = "";
			string pattern = @"([A-Z]{3})[\/]([A-Z]{3})\s([0-9]{1,10})[.]{0,1}([0-9]{0,10})";
			string currencyHelp = "currency";
			string exitString = "x";
			string errorMessage = "Bad input: ";
			string startMessage = "CURRENCY EXCHANGE CALCULATOR (DKK 100)" +
					Environment.NewLine + "To calculate currency exchange: [currency]/[currency] [amount]" +
					Environment.NewLine + "Currency list: " + currencyHelp +
					Environment.NewLine + "Exit: " + exitString + Environment.NewLine;
			#endregion

			CurrencyLoader currencyLoader = new CurrencyLoader();
			List<Currency> currencyList = currencyLoader.GetCurrencies();

			while (input != exitString)
			{
				Console.WriteLine(startMessage);

				input = Console.ReadLine();

				if (input == currencyHelp)
				{
					foreach (string currency in currencyLoader.GetCurrencyNameInfoList())
					{
						Console.WriteLine(currency);
					}
					Console.WriteLine();
				}
				else if (Regex.Match(input, pattern, RegexOptions.IgnoreCase).Success)
					Console.WriteLine(Environment.NewLine + 
						CalculationClass.Calculate(currencyList, input) + Environment.NewLine);
				else if (input != exitString)
					Console.WriteLine(errorMessage + input + Environment.NewLine);

			}

		}
	}
}
